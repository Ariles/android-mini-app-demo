package io.ariles.miniappdemo.domain.abstractions

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt

class EitherTest {

    @Test
    fun `succeeded "Either" only executes onSuccess lambda`() {
        //Given
        val executeOnSuccess: (Any) -> Unit = mock {}
        val executeOnFailure: (Failure) -> Unit = mock {}

        //When
        val success: Either<Any, *> = Either.Succeeded(mock())
        success.either(executeOnSuccess, executeOnFailure)

        //Then
        assertThat(success.isSuccess, equalTo(true))
        assertThat(success.isFailure, equalTo(false))

        verify(executeOnSuccess).invoke(any())
        verify(executeOnFailure, times(0)).invoke(any())
    }

    @Test
    fun `filed "Either" only executes onFailure lambda`() {
        //Given
        val executeOnSuccess: (Any) -> Unit = mock {}
        val executeOnFailure: (Failure) -> Unit = mock {}

        //When
        val failure: Either<*, Failure> = Either.Failed(mock())
        failure.either(executeOnSuccess, executeOnFailure)

        //Then
        assertThat(failure.isSuccess, equalTo(false))
        assertThat(failure.isFailure, equalTo(true))

        verify(executeOnSuccess, times(0)).invoke(anyInt())
        verify(executeOnFailure).invoke(any())
    }
}