package io.ariles.miniappdemo.domain.abstractions

sealed class Either<out T : Any, out R : Failure>(private val value: Any) {

    abstract fun getResult(): Any

    data class Succeeded<U : Any>(val successResult: U) : Either<U, Nothing>(successResult) {
        override fun getResult(): U = successResult
    }

    data class Failed<U : Failure>(val failureResult: U) : Either<Nothing, U>(failureResult) {
        override fun getResult(): U = failureResult
    }

    val isSuccess: Boolean
        get() = this is Succeeded

    val isFailure: Boolean
        get() = this is Failed

    fun either(doOnSuccess: ((T) -> Unit)?, doOnFailure: ((R) -> Unit)?) {
        when (this) {
            is Succeeded -> doOnSuccess?.invoke(this.getResult())
            is Failed -> doOnFailure?.invoke(this.getResult())
        }
    }
}