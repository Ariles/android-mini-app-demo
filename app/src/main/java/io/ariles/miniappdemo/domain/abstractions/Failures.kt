package io.ariles.miniappdemo.domain.abstractions

sealed class Failure

class NetworkFailure(httpCode: Int) : Failure()