package io.ariles.miniappdemo.domain.abstractions

interface IEntityMapper<FROM, TO> {
    fun map(from: FROM): TO
}